﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TextParserService_Module;

namespace TextParserService_TestModule
{
    public partial class Form1 : Form
    {
        private TextParserService textParserServer = new TextParserService();
        public Form1()
        {
            ServiceConfiguration.StartAsService = false;
            InitializeComponent();

            Initialize();
        }

        private void Initialize()
        {
            var arguments = Environment.GetCommandLineArgs().ToList();
            textParserServer.Start(arguments);
        }
    }
}
